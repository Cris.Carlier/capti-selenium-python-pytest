import logging

from selenium import webdriver


def init_driver(browser, base_url):
    logging.info("Iniciando sesion de " + browser)

    global driver

    if browser == "Firefox":
        driver = webdriver.Firefox()

    elif browser == "Chrome":
        driver = webdriver.Chrome()

    driver.maximize_window()
    driver.get(base_url)

    return driver


def get_driver():
    return driver

