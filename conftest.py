import os
import time
import pytest
import logging

from datetime import datetime

from page_objects import swag_labs_login, swag_labs_store
from config.config import init_driver, get_driver


def pytest_html_report_title(report):
    report.title = "Report " + datetime.now().strftime('%d-%m-%Y %H-%M-%S')


def pytest_configure(config):
    config._metadata = None
    if not os.path.exists('../reports'):
        os.makedirs('../reports')

    config.option.htmlpath = '../reports/Report ' + datetime.now().strftime("%d-%m-%Y %H-%M-%S") + ".html"


@pytest.fixture(scope="session")
def browser_setup():
    browser = "Chrome"
    base_url = "https://www.saucedemo.com/"
    driver = init_driver(browser, base_url)

    yield
    logging.info("Cerrando sesion de " + browser)
    time.sleep(1)
    driver.quit()


@pytest.fixture(scope="function")
def test_setup():
    base_url = "https://www.saucedemo.com/"
    get_driver().get(base_url)
    swag_labs_login.login_usuario("standard_user", "secret_sauce")

    yield
    logging.info("Cerrando sesion de usuario")
    time.sleep(1)
    swag_labs_store.log_out()
