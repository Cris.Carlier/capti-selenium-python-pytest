from page_objects import swag_labs_store, swag_labs_cart_checkout


def swag_ventas_simple(producto):
    swag_labs_store.agregar_producto_carro(producto)

    swag_labs_cart_checkout.carro_compra()
    swag_labs_cart_checkout.checkout_info("User", "Test", "99999")

    swag_labs_cart_checkout.checkout_overview()
    swag_labs_cart_checkout.checkout_complete()


def swag_ventas_multiple(productos):
    for producto in productos:
        swag_labs_store.agregar_producto_carro(producto)

    swag_labs_cart_checkout.carro_compra()
    swag_labs_cart_checkout.checkout_info("User", "Test", "99999")

    swag_labs_cart_checkout.checkout_overview()
    swag_labs_cart_checkout.checkout_complete()
