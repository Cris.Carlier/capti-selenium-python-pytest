import time

from selenium.webdriver.common.by import By
from config import config


def click(elemento):
    time.sleep(1)

    driver = config.get_driver()
    elemento_web = driver.find_element(By.XPATH, elemento)

    elemento_web.click()


def click_link(texto_link):
    time.sleep(1)

    driver = config.get_driver()
    elemento_web = driver.find_element(By.LINK_TEXT, texto_link)

    elemento_web.click()


def escribir(elemento, texto):
    time.sleep(1)

    driver = config.get_driver()
    elemento_web = driver.find_element(By.XPATH, elemento)

    elemento_web.clear()
    elemento_web.send_keys(texto)


def leer(elemento):
    time.sleep(1)

    driver = config.get_driver()
    elemento_web = driver.find_element(By.XPATH, elemento)

    return elemento_web.text


def validar_texto(elemento, esperado):
    time.sleep(1)

    driver = config.get_driver()
    elemento_web = driver.find_element(By.XPATH, elemento)

    encontrado = elemento_web.text
    assert encontrado == esperado, f"El texto esperado era '{esperado}', pero el texto real fue '{encontrado}'"
