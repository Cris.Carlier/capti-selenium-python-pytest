from selenium.webdriver.common.by import By
from config import config
from utilities import actions

product_title = '/html/body/div/div/div/div[2]/div/div/div[2]/div[1]'
product_add_to_cart = '//button[contains(text(),"Add to cart")]'
product_remove_cart = '//*[@id="remove-sauce-labs-backpack"]'
product_back_to_products = '//*[@id="back-to-products"]'
store_title = '/html/body/div/div/div/div[1]/div[2]/span'
store_menu = '//*[@id="react-burger-menu-btn"]'
store_logout = '//*[@id="logout_sidebar_link"]'


def agregar_producto_carro(producto):
    driver = config.get_driver()
    elemento_producto = driver.find_element(By.LINK_TEXT, producto)
    elemento_producto.click()

    actions.validar_texto(product_title, producto)
    actions.click(product_add_to_cart)
    actions.click(product_back_to_products)


def eliminar_producto_carro(producto):
    driver = config.get_driver()
    elemento_producto = driver.find_element(By.LINK_TEXT, producto)
    elemento_producto.click()

    actions.validar_texto(product_title, producto)
    actions.click(product_remove_cart)
    actions.click(product_back_to_products)


def log_out():
    actions.click(store_menu)
    actions.click(store_logout)
