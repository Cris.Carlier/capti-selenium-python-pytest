from utilities import actions

store_cart = '/html/body/div/div/div/div[1]/div[1]/div[3]/a'

cart_title = '/html/body/div/div/div/div[1]/div[2]/span'
cart_continue_shopping = '//*[@id="continue-shopping"]'
cart_checkout = '//*[@id="checkout"]'

checkout_title = '/html/body/div/div/div/div[1]/div[2]/span'
checkout_first_name = '//*[@id="first-name"]'
checkout_last_name = '//*[@id="last-name"]'
checkout_postal_code = '//*[@id="postal-code"]'
checkout_continue = '//*[@id="continue"]'
checkout_cancel = '//*[@id="cancel"]'
checkout_finish = '//*[@id="finish"]'
checkout_thank_you = '/html/body/div/div/div/div[2]/h2'
checkout_back_home = '//*[@id="back-to-products"]'

checkout_item_total = '/html/body/div/div/div/div[2]/div/div[2]/div[6]'
checkout_tax = '/html/body/div/div/div/div[2]/div/div[2]/div[7]'
checkout_total = '/html/body/div/div/div/div[2]/div/div[2]/div[8]'


def carro_compra():
    actions.click(store_cart)
    actions.validar_texto(cart_title, "Your Cart")


def checkout_info(first_name, last_name, postal_code):
    actions.click(cart_checkout)
    actions.validar_texto(checkout_title, "Checkout: Your Information")
    actions.escribir(checkout_first_name, first_name)
    actions.escribir(checkout_last_name, last_name)
    actions.escribir(checkout_postal_code, postal_code)


def checkout_overview():
    actions.click(checkout_continue)
    actions.validar_texto(checkout_title, "Checkout: Overview")


def checkout_complete():
    actions.click(checkout_finish)
    actions.validar_texto(checkout_title, "Checkout: Complete!")
    actions.validar_texto(checkout_thank_you, "Thank you for your order!")
    actions.click(checkout_back_home)


def obtener_total_items():
    valor = float(actions.leer(checkout_item_total).replace("Item total: $", ""))
    print(f"\nTotal Items: ${valor}")
    return valor


def obtener_tax():
    valor = float(actions.leer(checkout_tax).replace("Tax: $", ""))
    print(f"Tax: ${valor}")
    return valor


def obtener_total():
    valor = float(actions.leer(checkout_total).replace("Total: $", ""))
    print(f"Total: ${valor}")
    return valor
