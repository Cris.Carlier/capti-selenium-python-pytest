from utilities import actions

login_title = '/html/body/div/div/div[1]'
login_username = '//*[@id="user-name"]'
login_password = '//*[@id="password"]'
login_button = '//*[@id="login-button"]'
store_title = '/html/body/div/div/div/div[1]/div[2]/span'


def login_usuario(username, password):
    actions.validar_texto(login_title, "Swag Labs")
    actions.escribir(login_username, username)
    actions.escribir(login_password, password)
    actions.click(login_button)
    actions.validar_texto(store_title, "Products")
