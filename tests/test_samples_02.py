import pytest

from page_objects import swag_labs_login, swag_labs_store, swag_labs_cart_checkout


@pytest.mark.smoke
def test_smoke_1_login(browser_setup):
    swag_labs_login.login_usuario("standard_user", "secret_sauce")
    swag_labs_store.log_out()


@pytest.mark.smoke
def test_smoke_2_agregar_producto(browser_setup, test_setup):
    swag_labs_store.agregar_producto_carro("Sauce Labs Backpack")
    swag_labs_store.eliminar_producto_carro("Sauce Labs Backpack")


@pytest.mark.smoke
def test_smoke_3_finalizar_compra(browser_setup, test_setup):
    swag_labs_store.agregar_producto_carro("Sauce Labs Backpack")
    swag_labs_cart_checkout.carro_compra()
    swag_labs_cart_checkout.checkout_info("User", "Test", "99999")
    swag_labs_cart_checkout.checkout_overview()
    swag_labs_cart_checkout.checkout_complete()
