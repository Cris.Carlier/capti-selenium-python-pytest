import pytest

from utilities import swag_ventas


@pytest.mark.smoke
def test_smoke_3_multi_productos(browser_setup, test_setup):
    productos = ["Sauce Labs Backpack", "Sauce Labs Bike Light", "Sauce Labs Bolt T-Shirt", "Sauce Labs Fleece Jacket"]
    swag_ventas.swag_ventas_multiple(productos)
