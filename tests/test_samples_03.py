import pytest

from utilities.swag_ventas import swag_ventas_simple, swag_ventas_multiple
from page_objects import swag_labs_login, swag_labs_store, swag_labs_cart_checkout


@pytest.mark.regression
def test_regression_1(browser_setup, test_setup):
    swag_ventas_simple("Sauce Labs Backpack")


@pytest.mark.regression
@pytest.mark.parametrize("producto", ["Sauce Labs Backpack", "Sauce Labs Bike Light"])
def test_regression_2(browser_setup, test_setup, producto):
    swag_ventas_simple(producto)


@pytest.mark.regression
def test_regression_3(browser_setup, test_setup):
    swag_ventas_multiple(["Sauce Labs Backpack", "Sauce Labs Bike Light", "Sauce Labs Bolt T-Shirt", "Sauce Labs Onesie"])


@pytest.mark.smoke
def test_smoke_1_login(browser_setup):
    swag_labs_login.login_usuario("standard_user", "secret_sauce")
    swag_labs_store.log_out()


@pytest.mark.smoke
def test_smoke_2_agregar_producto(browser_setup, test_setup):
    swag_labs_store.agregar_producto_carro("Sauce Labs Backpack")
    swag_labs_store.eliminar_producto_carro("Sauce Labs Backpack")


@pytest.mark.smoke
def test_smoke_3_finalizar_compra(browser_setup, test_setup):
    swag_labs_store.agregar_producto_carro("Sauce Labs Backpack")
