import pytest

from selenium.webdriver.common.by import By
from utilities import actions
from config import config


@pytest.mark.smoke
def test_hardcode_1(browser_setup):
    # Logeamos con usuario y validamos que estemos en la poagina de inicio.
    actions.validar_texto('/html/body/div/div/div[1]', "Swag Labs")
    actions.escribir('//*[@id="user-name"]', "standard_user")
    actions.escribir('//*[@id="password"]', "secret_sauce")
    actions.click('//*[@id="login-button"]')
    actions.validar_texto('/html/body/div/div/div/div[1]/div[2]/span', "Products")

    # Buscamos el producto indicado.
    driver = config.get_driver()
    elemento_producto = driver.find_element(By.LINK_TEXT, "Sauce Labs Backpack")
    elemento_producto.click()

    # Luego de encontrarlo, lo agregamos al carro.
    actions.validar_texto('/html/body/div/div/div/div[2]/div/div/div[2]/div[1]', "Sauce Labs Backpack")
    actions.click('//button[contains(text(),"Add to cart")]')
    actions.click('//*[@id="back-to-products"]')

    # Abrimos el carro de compra
    actions.click('/html/body/div/div/div/div[1]/div[1]/div[3]/a')
    actions.validar_texto('/html/body/div/div/div/div[1]/div[2]/span', "Your Cart")

    # Procedemos con el checkout
    actions.click('//*[@id="checkout"]')
    actions.validar_texto('/html/body/div/div/div/div[1]/div[2]/span', "Checkout: Your Information")
    actions.escribir('//*[@id="first-name"]', "User")
    actions.escribir('//*[@id="last-name"]', "Test")
    actions.escribir('//*[@id="postal-code"]', "99999")
    actions.click('//*[@id="continue"]')
    actions.validar_texto('/html/body/div/div/div/div[1]/div[2]/span', "Checkout: Overview")

    # Finalizamos la compra
    actions.click('//*[@id="finish"]')
    actions.validar_texto('/html/body/div/div/div/div[1]/div[2]/span', "Checkout: Complete!")
    actions.validar_texto('/html/body/div/div/div/div[2]/h2', "Thank you for your order!")
    actions.click('//*[@id="back-to-products"]')
